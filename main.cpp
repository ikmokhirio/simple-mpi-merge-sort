#include <iostream>
#include <mpi/mpi.h>

using namespace std;

const int SIZE = 200000;

int *getPart(int *arr, int rank, bool isNotDiv, int procSize) {

    int *new_arr = nullptr;
    int j = 0;

    if (isNotDiv) {
        new_arr = new int[SIZE / procSize];
    } else {
        new_arr = new int[SIZE / procSize + SIZE % procSize];
    }

    int i = 0;
    for (i = rank; i < SIZE; i += procSize) {
        new_arr[j] = arr[i];
        j++;
    }

    if (procSize - 1 == rank) {
        for (i = SIZE - SIZE % procSize; i < SIZE; i++) {
            new_arr[j] = arr[i];
        }
    }

    return new_arr;
}

int *insertionSort(int *arr, int size) {
    for (int i = 1; i < size; i++) {
        for (int j = i; j > 0 && arr[j - 1] > arr[j]; j--) {
            int tmp = arr[j - 1];
            arr[j - 1] = arr[j];
            arr[j] = tmp;
        }
    }
    return arr;
}

int *getRandom(int size) {
    int *arr = new int[size];

    for (int i = 0; i < size; i++) {
        arr[i] = rand() % size;
    }

    return arr;
}

int *preparePhase(bool notFullyDivided, int rank, int procSize, int blockSize, MPI::Status &status, int *arr) {

    int **senderParts = nullptr;
    int *recvArray = nullptr;

    if (rank != 0 && rank != procSize - 1) { //Get from root node
        recvArray = new int[blockSize];
        MPI::COMM_WORLD.Recv(recvArray, blockSize, MPI::INT, 0, MPI::ANY_TAG, status);
    } else if (rank == procSize - 1 && rank != 0) {
        if (notFullyDivided) {
            recvArray = new int[blockSize + SIZE % procSize];
            MPI::COMM_WORLD.Recv(recvArray, blockSize + SIZE % procSize, MPI::INT, 0, MPI::ANY_TAG, status);
        } else {
            recvArray = new int[blockSize];
            MPI::COMM_WORLD.Recv(recvArray, blockSize, MPI::INT, 0, MPI::ANY_TAG, status);
        }
    } else { //Root node send parts of array to every node
        senderParts = new int *[procSize];
        for (int i = 0; i < procSize - 1; i++) {
            senderParts[i] = new int[blockSize];
        }
        if (notFullyDivided) {
            senderParts[procSize - 1] = new int[blockSize + SIZE % procSize];
        } else {
            senderParts[procSize - 1] = new int[blockSize];
        }
        for (int i = 0; i < procSize - 1; i++) {
            senderParts[i] = getPart(arr, i, notFullyDivided, procSize);
        }
        senderParts[procSize - 1] = getPart(arr, procSize - 1, notFullyDivided, procSize);
        if (procSize != 1) {
            for (int i = 1; i < procSize - 1; i++) {
                MPI::COMM_WORLD.Isend(senderParts[i], blockSize, MPI::INT, i, 0);
            }
            if (notFullyDivided) {
                MPI::COMM_WORLD.Isend(senderParts[procSize - 1], blockSize + SIZE % procSize, MPI::INT, procSize - 1,
                                      0);
            } else {
                MPI::COMM_WORLD.Isend(senderParts[procSize - 1], blockSize, MPI::INT, procSize - 1, 0);
            }
        }
        recvArray = new int[blockSize];
        recvArray = senderParts[0]; //Array for the Root Node
    }

    return recvArray;
}

int *getCombine(int rank, int procSize, int blockSize, bool isNotDiv, int *recvArray) {

    MPI::Status status;

    if (rank == 0) {
        int **arrays = new int *[procSize];
        for (int i = 0; i < procSize - 1; i++) {
            arrays[i] = new int[blockSize];
        }

        arrays[procSize - 1] = new int[blockSize + SIZE % procSize];

        for (int i = 0; i < blockSize; i++) {
            arrays[0][i] = recvArray[i];
        }

        for (int i = 1; i < procSize - 1; i++) {
            MPI::COMM_WORLD.Recv(arrays[i], blockSize, MPI::INT, i, 1, status);
        }

        MPI::COMM_WORLD.Recv(arrays[procSize - 1], blockSize + SIZE % procSize, MPI::INT, procSize - 1, 1, status);

        int *endArray = new int[SIZE];
        int j = 0;
        int i = 0;
        while (i < SIZE - SIZE % procSize) {
            for (int g = 0; g < procSize; g++) {
                endArray[i] = arrays[g][j];
                i++;
            }
            j++;
        }

        for (i = SIZE - SIZE % procSize; i < SIZE; i++) {
            endArray[i] = arrays[procSize - 1][j];
            j++;
        }

        return endArray;


    } else {
        if (rank == procSize - 1 && isNotDiv) {
            MPI::COMM_WORLD.Isend(recvArray, blockSize + SIZE % procSize, MPI::INT, 0, 1);
        } else {
            MPI::COMM_WORLD.Isend(recvArray, blockSize, MPI::INT, 0, 1);
        }
    }
    return nullptr;

}

int main(int argc, char *argv[]) {

    int *arr = getRandom(SIZE); // Generate random array with SIZE
    MPI::Status status;
    int procSize = 0;
    int rank = 0;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI::COMM_WORLD, &procSize); //Get number of processors
    MPI_Comm_rank(MPI::COMM_WORLD, &rank); //Get rank
    int blockSize = SIZE / procSize; //Get block size
    bool notFullyDivided = (SIZE % procSize != 0);

    double timer = MPI_Wtime();

    int *recvArray = preparePhase(notFullyDivided, rank, procSize, blockSize, status, arr); //Get array
    cout << "\nPreparation phase end\n" << endl;

    MPI::COMM_WORLD.Barrier();

    //Communication phase

    if (notFullyDivided && rank == procSize - 1) {
        recvArray = insertionSort(recvArray, blockSize + SIZE % procSize);
    } else {
        recvArray = insertionSort(recvArray, blockSize);
    }
    cout << "\nSort phase end\n" << endl;

    int *endArray = nullptr;

    if (procSize != 1) {
        endArray = getCombine(rank, procSize, blockSize, notFullyDivided, recvArray);
    } else {
        endArray = arr;
    }
    if (rank == 0) {
        endArray = insertionSort(endArray, SIZE);
    }

    timer = MPI_Wtime() - timer;

    if (endArray != nullptr && rank == 0) {
        for (int i = 0; i < SIZE; i++) {
            cout << " " << endArray[i] << endl;
        }

        cout << "Sort time : " << timer << endl;
    }


    //Output
    MPI::COMM_WORLD.Barrier();
    MPI_Finalize();

    return 0;
}
